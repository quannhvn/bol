if myHero.charName ~= "Blitzcrank" or not VIP_USER then return end
require "VPrediction"
require "Collision"

local Qrange, Qwidth, Qspeed, Qdelay = 900, 70, 1800, 0.25
local VP = nil
local Col = nil
local ForceTarget = nil
local Menu = nil

function OnLoad()
	Menu = scriptConfig("Blitzcrank", "Blitzcrank")
	Menu:addParam("Grab", "Grab!", SCRIPT_PARAM_ONKEYDOWN, false, 32)
	Menu:addParam("Grab2", "Grab Stunned/Dashing!", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("V"))
	Menu:addParam("Move", "Move to mouse", SCRIPT_PARAM_ONOFF, false)
	Menu:addParam("DrawQ", "Draw Q range", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("DrawP", "Draw predicted position", SCRIPT_PARAM_ONOFF, false)
	Menu:addParam("Lag", "Use Lag-Free circles", SCRIPT_PARAM_ONOFF, true)
	
	Menu:addSubMenu("Targets", "Targets")	
	for i=1, heroManager.iCount do
		local enemy = heroManager:GetHero(i)
		if enemy.team ~= myHero.team then
			Menu.Targets:addParam("D"..i, "Dont grab "..enemy.charName, SCRIPT_PARAM_ONOFF, false)
		end
	end
	
	VP = VPrediction() --Load VPrediction
	Col = Collision(Qrange, Qspeed, Qdelay, Qwidth+30)
end

function GetBestTarget(Range)
	local LessToKill = 100
	local LessToKilli = 0
	local target = nil
		
	for i=1, heroManager.iCount do
		local enemy = heroManager:GetHero(i)
		if ValidTarget(enemy, Range) then
			DamageToHero = myHero:CalcMagicDamage(enemy, 200)
			ToKill = enemy.health / DamageToHero
			if ((ToKill < LessToKill)  or (LessToKilli == 0)) and not Menu.Targets['D'..i] then
				LessToKill = ToKill
				LessToKilli = i
				target = enemy
			end
		end
	end
	
	return target
end

function OnTick()
	local target = GetBestTarget(Qrange)
	if Forcetarget ~= nil and ValidTarget(Forcetarget, Qrange) then
		target = Forcetarget	
	end
	
	if target and (myHero:CanUseSpell(_Q) == READY) and (Menu.Grab or Menu.Grab2) then
		MinHitChance = Menu.Grab and 1 or 2
		CastPosition,  HitChance, HeroPosition = VP:GetLineCastPosition(target, Qdelay, Qwidth, Qrange, Qspeed, myHero)
		if HitChance > MinHitChance and GetDistance(CastPosition) <= Qrange  then
			local Mcol = Col:GetMinionCollision(myHero, CastPosition)
			local Mcol2 = Col:GetMinionCollision(myHero, target)
			if not Mcol and not Mcol2 then
				CastSpell(_Q, CastPosition.x,  CastPosition.z)
			end
		end
	end
	
	if Menu.Move and (Menu.Grab or Menu.Grab2) then
		myHero:MoveTo(mousePos.x, mousePos.z)
	end
end

function OnDraw()
	if Menu.DrawQ then
		DrawCircle2(myHero.x, myHero.y, myHero.z, Qrange, ARGB(255, 0, 255, 0))
	end
	
	if Forcetarget ~= nil then
		DrawCircle2(Forcetarget.x, Forcetarget.y, Forcetarget.z, Qwidth, ARGB(255, 0, 255, 0))
	end
	
	if Menu.DrawP and HeroPosition and CastPosition then
		DrawCircle2(CastPosition.x, CastPosition.y, CastPosition.z, Qwidth, ARGB(255, 255, 0, 0))
		DrawCircle2(HeroPosition.x, HeroPosition.y, HeroPosition.z, Qwidth, ARGB(255, 0, 0, 255))
		
	end
end

function DrawCircleNextLvl(x, y, z, radius, width, color, chordlength)
	radius = radius or 300
	quality = math.max(8,math.floor(180/math.deg((math.asin((chordlength/(2*radius)))))))
	quality = 2 * math.pi / quality
	radius = radius*.92
	local points = {}
	for theta = 0, 2 * math.pi + quality, quality do
		local c = WorldToScreen(D3DXVECTOR3(x + radius * math.cos(theta), y, z - radius * math.sin(theta)))
		points[#points + 1] = D3DXVECTOR2(c.x, c.y)
	end
	DrawLines2(points, width or 1, color or 4294967295)
end


function DrawCircle2(x, y, z, radius, color)
	local vPos1 = Vector(x, y, z)
	local vPos2 = Vector(cameraPos.x, cameraPos.y, cameraPos.z)
	local tPos = vPos1 - (vPos1 - vPos2):normalized() * radius
	local sPos = WorldToScreen(D3DXVECTOR3(tPos.x, tPos.y, tPos.z))
	if not Menu.Lag then
		return DrawCircle(x, y, z, radius, color)
	end
	if OnScreen({ x = sPos.x, y = sPos.y }, { x = sPos.x, y = sPos.y })  then
		DrawCircleNextLvl(x, y, z, radius, 1, color, 75)	
	end
end

function OnWndMsg(Msg, Key)
	if Msg == WM_LBUTTONDOWN then
		local minD = 0
		local starget = nil
		for i, enemy in ipairs(GetEnemyHeroes()) do
			if ValidTarget(enemy) then
				if GetDistance(enemy, mousePos) <= minD or starget == nil then
					minD = GetDistance(enemy, mousePos)
					starget = enemy
				end
			end
		end
		
		if starget and minD < 500 then
			if Forcetarget and starget.charName == Forcetarget.charName then
				Forcetarget = nil
			else
				Forcetarget = starget
				print("<font color=\"#FF0000\">Blitzcrank: New target selected: "..starget.charName.."</font>")
			end
		end
	end
end

